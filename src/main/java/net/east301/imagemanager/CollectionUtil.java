package net.east301.imagemanager;


/**
 * Collection manipulation utilities.
 */
public final class CollectionUtil {

    /**
     * Gets index of the target element in the specified array.
     *
     * @param <T>       type of elements
     *
     * @param array     target array
     * @param target    target element
     *
     * @return  index of the target element in the target array, or -1 if the target is not found
     */
    public static <T> int indexOf(T[] array, T target) {
        if (array == null) {
            throw new IllegalArgumentException("`array` must not be null.");
        }

        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(target)) {
                return i;
            }
        }

        return -1;
    }

    /**
     * Private constructor.
     */
    private CollectionUtil() {
        // do nothing
    }

} // class CollectionUtil
