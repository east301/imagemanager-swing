package net.east301.imagemanager;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import javax.swing.tree.TreeNode;


/**
 * Tree node to represent file system hierarchy.
 */
public class LocationTreeNode implements TreeNode {

    /**
     * Initializes an instance of LocationTreeNode class.
     *
     * @param currentDirectory  path to the current directory
     */
    public LocationTreeNode(String currentDirectory) {
        this.currentDirectory = new File(currentDirectory).getAbsolutePath();
    }

    /**
     * @see TreeNode#getChildAt(int)
     */
    @Override
    public TreeNode getChildAt(int childIndex) {
        final String[] childDirs = getChildDirectories();
        return childIndex < childDirs.length ? new LocationTreeNode(childDirs[childIndex]) : null;
    }

    /**
     * @see TreeNode#getChildCount()
     */
    @Override
    public int getChildCount() {
        return getChildDirectories().length;
    }

    /**
     * @see TreeNode#getParent()
     */
    @Override
    public TreeNode getParent() {
        return this.currentDirectory.equals("/")
                ? new LocationTreeNode(new File(this.currentDirectory).getParent())
                : null;
    }

    /**
     * @see TreeNode#getIndex(TreeNode)
     */
    @Override
    public int getIndex(TreeNode node) {
        if (node instanceof LocationTreeNode) {
            final LocationTreeNode locationNode = (LocationTreeNode)node;
            return CollectionUtil.indexOf(getChildDirectories(), locationNode.getCurrentDirectory());
        }

        return -1;
    }


    /**
     * @see TreeNode#getAllowsChildren()
     */
    @Override
    public boolean getAllowsChildren() {
        return true;
    }

    /**
     * @see TreeNode#isLeaf()
     */
    @Override
    public boolean isLeaf() {
        return getChildDirectories().length == 0;
    }

    /**
     * @see TreeNode#children()
     */
    @Override
    public Enumeration<?> children() {
        final ArrayList<LocationTreeNode> childNodes = new ArrayList<LocationTreeNode>();
        for (String childPath : getChildDirectories()) {
            childNodes.add(new LocationTreeNode(childPath));
        }

        return Collections.enumeration(childNodes);
    }

    /**
     * @see Object#toString()
     */
    @Override
    public String toString() {
        return this.currentDirectory.equals("/") ? "/" : new File(this.currentDirectory).getName();
    }

    /**
     * Gets path to the current directory.
     *
     * @return  path to the current directory
     */
    public String getCurrentDirectory() {
        return this.currentDirectory;
    }

    /**
     * Gets child directories.
     *
     * @return  child directoriess
     */
    private synchronized String[] getChildDirectories() {
        if (this.childDirectories == null) {
            this.childDirectories = FileSystemUtil.getChildDirectories(this.currentDirectory);
        }

        return this.childDirectories;
    }

    /**
     * Current directory.
     */
    private String currentDirectory;

    /**
     * Child directory cache.
     */
    private String[] childDirectories;

} // class LocationTreeNode
