package net.east301.imagemanager;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFrame;
import net.infonode.docking.RootWindow;
import net.infonode.docking.SplitWindow;
import net.infonode.docking.TabWindow;
import net.infonode.docking.View;
import net.infonode.docking.ViewSerializer;
import net.infonode.docking.theme.DockingWindowsTheme;
import net.infonode.docking.theme.ShapedGradientDockingTheme;
import net.infonode.docking.util.DockingUtil;
import net.infonode.docking.util.MixedViewHandler;
import net.infonode.docking.util.ViewMap;


/**
 * Main window of the program.
 */
public class MainWindow extends JFrame {

    /**
     * Initializes an instance of MainWindow class.
     */
    public MainWindow() {
        initializeComponents();
        setSize(new Dimension(800, 600));
    }

    /**
     * Initializes GUI components on the main window.
     */
    private void initializeComponents() {
        // create dock-able windows
        final TabWindow tabWindow = new TabWindow();

        final SplitWindow splitWindow1 = new SplitWindow(
                false,
                0.5f,
                new View("Query", null, new QueryPanel()),
                new View("Group", null, new GroupPanel()));

        final SplitWindow splitWindow0 = new SplitWindow(true, 0.7f, tabWindow, splitWindow1);

        // creates root window
        final ViewMap viewMap = new ViewMap();
        final MixedViewHandler handler = new MixedViewHandler(viewMap, new ViewSerializer() {
            @Override
            public void writeView(View view, ObjectOutputStream stream) throws IOException {
                throw new UnsupportedOperationException();
            }

            @Override
            public View readView(ObjectInputStream stream) throws IOException {
                throw new UnsupportedOperationException();
            }
        });

        final RootWindow rootWindow = DockingUtil.createRootWindow(viewMap, handler, true);
        rootWindow.setWindow(splitWindow0);

        // adds it to content pane of the JFrame
        getContentPane().add(rootWindow, BorderLayout.CENTER);

        // change style of docking window
        final ShapedGradientDockingTheme theme = new ShapedGradientDockingTheme();
        rootWindow.getRootWindowProperties().addSuperObject(theme.getRootWindowProperties());

        //
        this.rootWindow = rootWindow;
        this.contentTabWindow = tabWindow;
        this.currentTheme = theme;
    }

    /**
     * Root window.
     */
    private RootWindow rootWindow;

    /**
     * Content tab.
     */
    private TabWindow contentTabWindow;

    /**
     * Current theme of docking window.
     */
    private DockingWindowsTheme currentTheme;

} // class MainWindow
