package net.east301.imagemanager;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;


/**
 * File system manipulation utilities.
 */
public final class FileSystemUtil {

    /**
     * Gets list of child directories in the specified directory.
     *
     * @param target    target directory
     *
     * @return  array of child directories
     */
    public static String[] getChildDirectories(String target) {
        if (target == null) {
            throw new IllegalArgumentException("`target` must not be null.");
        }

        final ArrayList<String> result = new ArrayList<String>();

        final String[] childDirNames = new File(target).list();
        if (childDirNames != null) {
            for (String childDirName : childDirNames) {
                final File childDir = new File(target, childDirName);

                if (childDir.isDirectory() && !childDir.isHidden()) {
                    result.add(childDir.getAbsolutePath());
                }
            }
        }

        Collections.sort(result);
        return result.toArray(new String[0]);
    }

    /**
     * Private constructor.
     */
    private FileSystemUtil() {
        // do nothing
    }

} // class FileSystemUtil
