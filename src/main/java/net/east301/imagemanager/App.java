package net.east301.imagemanager;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;


/**
 * Entry point of the application.
 */
public class App {

    /**
     * Entry point of the application.
     *
     * @param args  command line arguments
     */
    public static void main(String[] args) {
        // sets look and feel
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }

        // shows main window
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                final MainWindow wnd = new MainWindow();

                wnd.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                wnd.setVisible(true);
            }
        });
    }

} // class App
