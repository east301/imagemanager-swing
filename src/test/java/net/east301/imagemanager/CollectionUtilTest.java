package net.east301.imagemanager;

import org.junit.Assert;
import org.junit.Test;


/**
 * Tests methods in CollectionUtil class.
 */
public class CollectionUtilTest {

    /**
     * {@link CollectionUtil#indexOf(T[], Object)} throws an exception
     * if {@code null} is specified as {@code array} parameter.
     */
    @Test(expected = IllegalArgumentException.class)
    public void indexOf_specifiesNullAsArray_throwsException() {
        CollectionUtil.indexOf((String[])null, "foo");
    }

    /**
     * {@link CollectionUtil#indexOf(T[], Object)} returns correct results.
     */
    @Test
    public void indexOf_returnsCorrectResults() {
        Assert.assertEquals(0, CollectionUtil.indexOf(new Integer[] { 1, 2, 3 }, 1));
        Assert.assertEquals(1, CollectionUtil.indexOf(new Integer[] { 1, 2, 3 }, 2));
        Assert.assertEquals(2, CollectionUtil.indexOf(new Integer[] { 1, 2, 3 }, 3));

        Assert.assertEquals(-1, CollectionUtil.indexOf(new Integer[] { 1, 2, 3 }, 0));
        Assert.assertEquals(-1, CollectionUtil.indexOf(new Integer[] { 1, 2, 3 }, 4));
        Assert.assertEquals(-1, CollectionUtil.indexOf(new Integer[0], 0));

        Assert.assertEquals(0, CollectionUtil.indexOf(new String[] { "foo", "bar", "baz" }, "foo"));
        Assert.assertEquals(1, CollectionUtil.indexOf(new String[] { "foo", "bar", "baz" }, "bar"));
        Assert.assertEquals(2, CollectionUtil.indexOf(new String[] { "foo", "bar", "baz" }, "baz"));

        Assert.assertEquals(-1, CollectionUtil.indexOf(new String[] { "foo", "bar", "baz" }, "hoge"));
        Assert.assertEquals(-1, CollectionUtil.indexOf(new String[] { "foo", "bar", "baz" }, null));
        Assert.assertEquals(-1, CollectionUtil.indexOf(new String[0], "hoge"));
    }

} // class CollectionUtilTest
